package com.example.demo;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedDelay = 5000)
    public void execFixedDelay() {
        System.out.println("実行完了の5000msに次を実行");
    }

    @Scheduled(fixedRate = 5000)
    public void execFixedRate() {
        System.out.println("実行開始の5000msに次を実行");
    }

    @Scheduled(initialDelay = 60000, fixedRate = 5000)
    public void execInitialDelay() {
        System.out.println("boot起動開始から60000ms後に実行、その後は実行開始の5000msに次を実行");
    }

    @Scheduled(cron = "0 * * * * *", zone = "Asia/Tokyo")
    public void execCron() {
        System.out.println("cron設定で定期実行");
    }
}